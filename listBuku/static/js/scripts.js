$(document).ready(function(){
    searchingBooks("Programming Book");
    $("#searchbarinput").change(function(){
        searchingBooks(document.getElementById("searchbarinput").value);
    });
});

function searchingBooks(toSearch) {
    $.getJSON(`https://www.googleapis.com/books/v1/volumes?q=${toSearch}`, function(result){
        let index = 1;
        document.getElementsByTagName("table")[0].innerHTML = 
`
<thead>
<tr>
    <th scope="col">No.</th>
    <th scope="col">Cover</th>
    <th scope="col">Title</th>
    <th scope="col">Authors</th>
</tr>
</thead>
<tbody>
</tbody>
`;
         for(let i = 0; i<10;i++){
            console.log(result)
            let row = document.createElement("tr");

            let data0 = document.createElement("td");
            data0.innerText = index;
            row.appendChild(data0);

            let data1 = document.createElement("td");
            data1.innerHTML = "<img src = " + result.items[i].volumeInfo.imageLinks.smallThumbnail + ">";
            row.appendChild(data1);

            let data2 = document.createElement("td");
            data2.innerText = result.items[i].volumeInfo.title;
            row.appendChild(data2);
            
            let data3 = document.createElement("td");
            data3.innerText = result.items[i].volumeInfo.authors;
            row.appendChild(data3);            

            document.getElementsByTagName("tbody")[0].appendChild(row);
            ++index;
        }
    });
}
