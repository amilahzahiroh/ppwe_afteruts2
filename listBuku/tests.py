from django.test import TestCase, Client
from django.urls import resolve
from .views import *
#Imports for selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

#UNIT TESTS
class listBuku(TestCase):
	def test_url_existance_true(self):
		response = self.client.get('')
		self.assertEqual(response.status_code,200)

	def test_url_existance_false(self):
		response = Client().get('/invalid/')
		self.assertEqual(response.status_code,404)

	def test_is_template_listBukuPage_html_used(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'listBukuPage.html')

	def test_h1_halaman(self):
		response = Client().get('')
		content = response.content.decode('UTF8')
		self.assertIn('WINDOWS TO INFINITY', content)

	