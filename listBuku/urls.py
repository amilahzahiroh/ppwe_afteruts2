from django.urls import path
from . import views

app_name = 'listBuku'

urlpatterns = [
    path('', views.listBukuPage, name='listBuku'),
]